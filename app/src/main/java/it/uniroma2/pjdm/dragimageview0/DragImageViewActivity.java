package it.uniroma2.pjdm.dragimageview0;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class DragImageViewActivity extends AppCompatActivity {

    private View selected_item = null;
    private int offset_x = 0;
    private int offset_y = 0;

    private String myTAG = "DragImageViewActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_image_view);

        View.OnTouchListener otl = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    offset_x = (int) motionEvent.getX();
                    offset_y = (int) motionEvent.getY();
                    selected_item = view;
                }
                return false;
            }
        };

        ImageView iv = findViewById(R.id.sheep);
        iv.setOnTouchListener(otl);

        ImageView iv2 = findViewById(R.id.wolf);
        iv2.setOnTouchListener(otl);

        RelativeLayout myLayout = findViewById(R.id.myLayout);
        myLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(selected_item == null)
                    return false;

                if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {

                    int x = (int) motionEvent.getX() - offset_x;
                    int y = (int) motionEvent.getY() - offset_y;

                    int maxwidth = view.getWidth() - 300;
                    int maxheight = view.getHeight() - 300;

                    if(x > maxwidth)
                        x = maxwidth;
                    if(x < 0)
                        x = 0;
                    if(y > maxheight)
                        y = maxheight;
                    if(y < 0)
                        y = 0;

                    Log.d(myTAG, "x: " + x + "/" + maxwidth + " y: " + y + "/" + maxheight);

                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            new ViewGroup.MarginLayoutParams(
                                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT
                            )
                    );
                    lp.setMargins(x, y, 0, 0);
                    selected_item.setLayoutParams(lp);
                } else if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    selected_item = null;
                }
                return true;
            }
        });
    }
}
