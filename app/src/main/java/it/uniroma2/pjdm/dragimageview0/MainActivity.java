package it.uniroma2.pjdm.dragimageview0;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchDragImageViewActivity(View view) {
        Intent intent = new Intent(this, DragImageViewActivity.class);
        startActivity(intent);
    }

    public void launchDragImageActivity(View view) {
        Intent intent = new Intent(this, DragImageActivity.class);
        startActivity(intent);
    }
}
