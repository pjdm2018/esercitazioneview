package it.uniroma2.pjdm.dragimageview0;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by clauz on 5/6/18.
 */

public class MyView extends View {
    private int x = 100;
    private int y = 100;
    private Bitmap img = null;
    private boolean dragOn = false;
    private int offsetx = 0;
    private int offsety = 0;

    private void localInit(Context context) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        img = BitmapFactory.decodeResource(context.getResources(), R.drawable.sheep);
        offsetx = img.getWidth() / 2;
        offsety = img.getHeight() / 2;
        Log.d("PJDM", "offsetx " + offsetx + " offsety " + offsety);
    }

    public MyView(Context context) {
        super(context);
        localInit(context);
    }

    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        localInit(context);
    }

    public MyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        localInit(context);
    }

    public MyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        localInit(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(img, x, y, null);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // return super.onTouchEvent(event);
        int eventaction = event.getAction();
        int tx = (int) event.getX() - offsetx;
        int ty = (int) event.getY() - offsety;

        switch(eventaction) {
            case MotionEvent.ACTION_DOWN:
                /* check if the event is in the boundaries of the bitmap */
                if(tx > x && tx < x + img.getWidth() && ty > y && ty < y + img.getHeight()) {
                    dragOn = true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                /* move the bitmap: update coordinates and redraw*/
                x = tx;
                y = ty;
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                /* the motion event is over*/
                dragOn = false;
                break;
        }

        return true;
    }
}
